using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ser_api.Services;
using ser_api.Models;

namespace ser_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExchangeRateController : ControllerBase
    {

        private ILogger _logger;
        private IExchangeRateService _service;

        public ExchangeRateController(ILogger<ExchangeRateController> logger, IExchangeRateService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet("/api/exchangerates/{month}")]
        public ActionResult<List<ExchangeRateDTO>> GetExchangeRates(string month)
        {
            try
            {
                return Ok(_service.GetExchangeRates(month));
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }
        }

        [HttpGet("/api/currentexchange")]
        public ActionResult<ExchangeRateDTO> GetExchangeRate()
        {
            try
            {
                return Ok(_service.GetExchangeRate());
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }
        }

        [HttpPost("/api/exchangerates")]
        public ActionResult<ExchangeRateDTO> AddExchangeRate(ExchangeRateDTO exchangeratedto)
        {
            try
            {
                var result = _service.AddExchangeRate(exchangeratedto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }

        [HttpPut("/api/exchangerates")]
        public ActionResult<ExchangeRateDTO> UpdateExchangeRate(string id, ExchangeRateDTO exchangeratedto)
        {
            try
            {
                // if(id != exchangeratedto.Id){
                //     throw new Exception("El parametro el endpoint no corresponde con el id o nombre del tipo de cambio");
                // }
                var result = _service.UpdateExchangeRate(id, exchangeratedto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }

        [HttpDelete("/api/exchangerates/{id}")]
        public ActionResult<string> DeleteExchangeRate(string id)
        {
            try
            {
                _service.DeleteExchangeRate(id);
                //_logger.LogInformation("exchangerates", _exchangerates);
                return Ok(id);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
        }
    }
}