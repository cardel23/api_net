using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ser_api.Services;
using ser_api.Models;

namespace ser_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {

        private ILogger _logger;
        private IProductService _service;


        public ProductController(ILogger<ProductController> logger, IProductService service)
        {
            _logger = logger;
            _service = service;

        }

        [HttpGet("/api/products")]
        public ActionResult<List<ProductDTO>> GetProducts()
        {
            try
            {
                return Ok(_service.GetProducts());
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }
        }

        [HttpGet("/api/products/{id}")]
        public ActionResult<ProductDTO> GetProduct(string id){
            try
            {
                return Ok(_service.GetProduct(id));
            }
            catch (Exception e)
            {
                if(e.InnerException != null){
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
        }

        [HttpPost("/api/products")]
        public ActionResult<ProductDTO> AddProduct(ProductDTO productdto)
        {
            try
            {
                var result = _service.AddProduct(productdto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if(e.InnerException != null){
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch(Exception e){ return ValidationProblem(e.Message);}
        }

        [HttpPut("/api/products")]
        public ActionResult<ProductDTO> UpdateProduct(string id, ProductDTO productdto)
        {
            try
            {

                var result = _service.UpdateProduct(id, productdto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }

        [HttpDelete("/api/products/{id}")]
        public ActionResult<string> DeleteProduct(string id)
        {
            try
            {
                _service.DeleteProduct(id);
                return Ok(id);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }
    }
}