using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ser_api.Services;
using ser_api.Models;

namespace ser_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InvoiceController : ControllerBase
    {

        private ILogger _logger;
        private IInvoiceService _service;


        public InvoiceController(ILogger<InvoiceController> logger, IInvoiceService service)
        {
            _logger = logger;
            _service = service;

        }

        [HttpGet("/api/invoices")]
        public ActionResult<List<InvoiceDTO>> GetInvoices()
        {
            try
            {
                return Ok(_service.GetInvoices());
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }
        }

        [HttpGet("/api/invoices/{id}")]
        public ActionResult<InvoiceDTO> GetInvoice(string id){
            try
            {
                return Ok(_service.GetInvoice(id));
            }
            catch (Exception e)
            {
                if(e.InnerException != null){
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
        }

        [HttpGet("/api/invoices/client/{id}")]
        public ActionResult<InvoiceDTO> GetClientInvoices(string id){
            try
            {
                return Ok(_service.GetClientInvoices(id));
            }
            catch (Exception e)
            {
                if(e.InnerException != null){
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
        }

        [HttpPost("/api/invoices")]
        public ActionResult<InvoiceDTO> AddInvoice(InvoiceDTO invoicedto)
        {
            try
            {
                var result = _service.AddInvoice(invoicedto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if(e.InnerException != null){
                    var ex = e.InnerException;
                    ex.ToString();
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch(Exception e){ return ValidationProblem(e.Message);}
        }

        [HttpPut("/api/invoices")]
        public ActionResult<InvoiceDTO> UpdateInvoice(string id, InvoiceDTO invoicedto)
        {
            try
            {
                // if (id != invoicedto.SKU)
                // {
                //     throw new Exception("El parametro el endpoint no corresponde con el SKU del invoiceo");
                // }
                var result = _service.UpdateInvoice(id, invoicedto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }

        [HttpDelete("/api/invoices/{id}")]
        public ActionResult<string> DeleteInvoice(string id)
        {
            try
            {
                _service.DeleteInvoice(id);
                return Ok(id);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }

        [HttpGet("/api/report")]
        public void GetReport(){
            _service.getReport();
        }
    }
}