using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ser_api.Services;
using ser_api.Models;

namespace ser_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClientController : ControllerBase
    {

        private ILogger _logger;
        private IClientService _service;


        public ClientController(ILogger<ClientController> logger, IClientService service)
        {
            _logger = logger;
            _service = service;

        }

        [HttpGet("/api/clients")]
        public ActionResult<List<ClientDTO>> GetClients()
        {
            try
            {  
                // JObject o = new JObject();
                // o["result"] = "";
                // o
                // o.Add("result",_service.GetClients())
                return Ok(_service.GetClients());
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }

        }

        [HttpGet("/api/clients/{id}")]
        public ActionResult<ClientDTO> GetClient(string id){
            try
            {
                return Ok(_service.GetClient(id));
            }
            catch (Exception e)
            {
                if(e.InnerException != null){
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
        }

        [HttpPost("/api/clients")]
        public ActionResult<ClientDTO> AddClient(ClientDTO clientdto)
        {
            try
            {
                var result = _service.AddClient(clientdto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }

        [HttpPut("/api/clients")]
        public ActionResult<ClientDTO> UpdateClient(string id, ClientDTO clientdto)
        {
            try
            {
                var result = _service.UpdateClient(id, clientdto);
                if (result != null)
                    return Ok(result);
                else
                    return ValidationProblem();
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }
        }

        [HttpDelete("/api/clients/{id}")]
        public ActionResult<string> DeleteClient(string id)
        {
            try
            {
                _service.DeleteClient(id);
                //_logger.LogInformation("clients", _clients);
                return Ok(id);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                if (e.InnerException != null)
                {
                    return ValidationProblem(e.InnerException.Message);
                }
                return ValidationProblem(e.Message);
            }
            catch (Exception e) { return ValidationProblem(e.Message); }

        }
    }
}