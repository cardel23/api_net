using System.Collections.Generic;
using ser_api.Models;

namespace ser_api.Services
{
    public interface IExchangeRateService
    {
        
        public List<ExchangeRateDTO> GetExchangeRates(string month);
        
        public ExchangeRateDTO GetExchangeRate(string id);
        
        public ExchangeRateDTO GetExchangeRate();
        
        public ExchangeRateDTO AddExchangeRate(ExchangeRateDTO ExchangeRateItem);

        public ExchangeRateDTO UpdateExchangeRate(string id, ExchangeRateDTO ExchangeRateItem);

        public string DeleteExchangeRate(string id);
    }
    
}