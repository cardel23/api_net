using System.Collections.Generic;
using ser_api.Models;

namespace ser_api.Services
{
    public interface IProductService
    {
        
        public List<ProductDTO> GetProducts();
        public ProductDTO GetProduct(string id);
        public ProductDTO AddProduct(ProductDTO productItem);

        public ProductDTO UpdateProduct(string id, ProductDTO productItem);

        public string DeleteProduct(string id);
    }
    
}