using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using ser_api.Models;

namespace ser_api.Services
{
    public class ProductService : IProductService
    {

        private DBContext db;
        public ProductService(DBContext _db)
        {
            db = _db;
        }

        public List<ProductDTO> GetProducts()
        {
            try
            {
                var list = db.Products.ToList();
                List<ProductDTO> dTOs = new List<ProductDTO>();
                try
                {
                    ExchangeRateDTO exRate;
                    exRate = new ExchangeRateService(db).GetExchangeRate();
                    list.ForEach(e => dTOs.Add(e.toDTO(exRate)));
                }
                catch (System.Exception)
                {
                    foreach (var item in list)
                    {   
                        var d = item.toDTO();
                        d.ExchangeRate = "No hay tipo de cambio válido para hoy";
                        dTOs.Add(d);
                    }
                }
                return dTOs;
                
            }
            catch (Exception e) { throw e; }
        }

        public ProductDTO GetProduct(string id)
        {
            try
            {
                var p = db.Products.Where(p => p.SKU == id || p.Name == id).First();
                if (p == null)
                    return null;
                try
                {
                    ExchangeRateDTO exRate;
                    exRate = new ExchangeRateService(db).GetExchangeRate();
                    return p.toDTO(exRate);
                }
                catch (System.Exception)
                {
                    var d = p.toDTO();
                    d.ExchangeRate = "No hay tipo de cambio válido para hoy";
                    return d;
                }
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe cliente con el id " + id); }
            catch (Exception e) { throw e; }
        }

        public ProductDTO AddProduct(ProductDTO productItem)
        {
            try
            {
                Product p = newProduct(productItem);
                db.Products.Add(p);
                db.SaveChanges();
                try
                {
                    ExchangeRateDTO exRate;
                    exRate = new ExchangeRateService(db).GetExchangeRate();
                    return p.toDTO(exRate);
                }
                catch (System.Exception)
                {
                    var d = p.toDTO();
                    d.ExchangeRate = "No hay tipo de cambio válido para hoy";
                    return d;
                }


            }
            catch (DbUpdateException e) { throw e; }
            catch (Exception e) { throw e; }
        }

        public ProductDTO UpdateProduct(string id, ProductDTO productItem)
        {
            try
            {
                var old = db.Products.Where(p => p.SKU == productItem.SKU).First();
                if (old == null)
                {
                    throw new Exception("No existe producto con el SKU " + productItem.SKU);
                }
                Product p = productToUpdate(old, productItem);
                p.Updated = DateTime.Now;
                db.Products.Update(p);
                db.SaveChanges();
                try
                {
                    ExchangeRateDTO exRate;
                    exRate = new ExchangeRateService(db).GetExchangeRate();
                    return p.toDTO(exRate);
                }
                catch (System.Exception)
                {
                    var d = p.toDTO();
                    d.ExchangeRate = "No hay tipo de cambio válido para hoy";
                    return d;
                }

            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe producto con el id " + id); }
            catch (Exception e) { throw e; }
        }

        public string DeleteProduct(string id)
        {
            try
            {
                var productdto = db.Products.Where(p => p.SKU == id).First();
                if (productdto == null)
                    throw new Exception("No existe producto con el id " + id);
                db.Products.Remove(productdto);
                db.SaveChanges();
                return id;
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe producto con el id " + id); }
            catch (Exception e) { throw e; }
        }

        private Product newProduct(ProductDTO productDTO)
        {
            Product p = new Product();
            if (productDTO.Description != null)
                p.Description = productDTO.Description;
            if (productDTO.Id != null)
                p.Id = productDTO.Id;
            if (productDTO.SKU != null)
                p.SKU = productDTO.SKU;
            if (productDTO.Name != null)
                p.Name = productDTO.Name;
            p.NormalPrice = productDTO.NormalPrice;
            p.IsDollarPrice = productDTO.IsDollarPrice;
            p.Active = productDTO.Active;
            return p;
        }

        private Product productToUpdate(Product p, ProductDTO productDTO)
        {
            if (productDTO.Description != p.Description)
                p.Description = productDTO.Description;
            if (productDTO.SKU != p.SKU)
                p.SKU = productDTO.SKU;
            if (productDTO.Name != p.Name)
                p.Name = productDTO.Name;
            if (productDTO.NormalPrice != p.NormalPrice)
                p.NormalPrice = productDTO.NormalPrice;
            if (productDTO.IsDollarPrice != p.IsDollarPrice)
                p.IsDollarPrice = productDTO.IsDollarPrice;
            if (productDTO.Active != p.Active)
                p.Active = productDTO.Active;
            return p;
        }
    }
}
