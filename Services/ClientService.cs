using System;
using System.Collections.Generic;
using ser_api.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
namespace ser_api.Services
{
    public class ClientService : IClientService
    {
        DBContext db;
        public ClientService(DBContext context)
        {
            this.db = context;
        }
        public List<ClientDTO> GetClients()
        {
            try
            {
                var list = db.Clients.ToList();
                List<ClientDTO> dTOs = new List<ClientDTO>();
                list.ForEach(e => dTOs.Add(e.toDTO()));
                return dTOs;
            }
            catch (Exception e) { throw e; }
        }

        public ClientDTO GetClient(string id)
        {
            try
            {
                var p = db.Clients.Where(p => p.Code == id || p.Name == id).First();
                if (p != null)
                    return p.toDTO();
                else return null;
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe cliente con el id " + id); }
            catch (Exception e) { throw e; }
        }

        public ClientDTO AddClient(ClientDTO clientItem)
        {
            try
            {
                if(!CheckClient(clientItem))
                    throw new Exception("Datos incorrectos");
                Client p = newClient(clientItem);
                db.Clients.Add(p);
                db.SaveChanges();
                return p.toDTO();
            }
            catch (DbUpdateException e) { throw e; }
            catch (Exception e) { throw e; }
        }

        public ClientDTO UpdateClient(string id, ClientDTO clientItem)
        {
            try
            {
                var old = db.Clients.Where(p => p.Id == clientItem.Id).First();
                if (old == null)
                {
                    throw new Exception("No existe cliente con el id " + clientItem.Id);
                }
                Client p = clientToUpdate(old, clientItem);
                p.Updated = DateTime.Now;
                db.Clients.Update(p);
                db.SaveChanges();
                return p.toDTO();
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe cliente con el id " + id); }
            catch (Exception e) { throw e; }
        }

        public string DeleteClient(string id)
        {
            try
            {
                var clientdto = db.Clients.Where(p => p.Id == id).First();
                if (clientdto == null)
                    throw new Exception("No existe cliente con el id " + id);
                db.Clients.Remove(clientdto);
                db.SaveChanges();
                return id;
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe cliente con el id " + id); }
            catch (Exception e) { throw e; }
        }


        private Client newClient(ClientDTO dto)
        {
            Client c = new Client();
            if (CheckClient(dto))
                c.Name = dto.Name;
            return c;
        }

        private Client clientToUpdate(Client c, ClientDTO dto)
        {
            if (CheckClient(dto) && dto.Name != c.Name)
                c.Name = dto.Name;
                c.Code = dto.Code;
            return c;
        }

        private bool CheckClient(ClientDTO dto)
        {
            if (dto != null && dto.Name != "" && dto.Code != null && dto.Code !="")
                return true;
            else return false;
        }
    }

}