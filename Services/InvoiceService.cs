using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using ser_api.Models;
using Newtonsoft.Json.Linq;


namespace ser_api.Services
{
    public class InvoiceService : IInvoiceService
    {

        private DBContext db;
        public InvoiceService(DBContext _db)
        {
            db = _db;
        }

        public List<InvoiceDTO> GetInvoices()
        {
            try
            {
                var list = db.Invoices.Include(e => e.ExchangeRate).Include(e => e.InvoiceLines)
                .Include(e=>e.Client).ToList();
                List<InvoiceDTO> dTOs = new List<InvoiceDTO>();
                foreach (var item in list)
                {
                    var dto = item.toDTO();
                    dto.invoiceLines = GetDTOLines(item);
                    dTOs.Add(dto);
                }
                return dTOs;
            }
            catch (Exception e) { throw e; }
        }

        public List<InvoiceDTO> GetClientInvoices(string clientName)
        {
            try
            {
                var list = db.Invoices.Where(e => e.Client.Name == clientName || e.Client.Code == clientName)
                .Include(e => e.ExchangeRate).Include(e => e.InvoiceLines)
                .Include(e=>e.Client).ToList();
                List<InvoiceDTO> dTOs = new List<InvoiceDTO>();
                foreach (var item in list)
                {
                    var dto = item.toDTO();
                    dto.invoiceLines = GetDTOLines(item);
                    dTOs.Add(dto);
                }
                return dTOs;
            }
            catch (Exception e) { throw e; }
        }

        public InvoiceDTO GetInvoice(string id)
        {
            try
            {
                var p = db.Invoices.Where(p => p.DocumentNo == id).Include(e => e.InvoiceLines).First();
                if (p != null)
                {
                    var dto = p.toDTO();
                    dto.invoiceLines = GetDTOLines(p);
                    return p.toDTO();
                }
                else return null;
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No se han encontrado facturas"); }
            catch (Exception e) { throw e; }
        }


        public InvoiceDTO AddInvoice(InvoiceDTO invoiceItem)
        {
            try
            {
                if (!CheckInvoice(invoiceItem))
                {
                    throw new Exception("Uno o mas valores no válidos en cabecera de factura");
                }
                Invoice p = newInvoice(invoiceItem);
                db.Invoices.Add(p);
                db.SaveChanges();
                var l = GetDTOLines(p);
                var res = p.toDTO();
                res.invoiceLines = l;
                return res;
            }
            catch (DbUpdateException e) { throw e; }
            catch (Exception e) { throw e; }
        }

        public InvoiceDTO UpdateInvoice(string id, InvoiceDTO invoiceItem)
        {
            try
            {
                if (!CheckInvoice(invoiceItem))
                {
                    throw new Exception("Uno o mas valores no válidos en cabecera de factura");
                }
                var old = db.Invoices.Where(e => e.DocumentNo == invoiceItem.DocumentNo).Include(e => e.InvoiceLines).FirstOrDefault();
                if (old == null)
                {
                    throw new Exception("No existe factura con el id " + invoiceItem.DocumentNo);
                }
                Invoice p = invoiceToUpdate(old, invoiceItem);
                processTotals(p);
                p.Updated = DateTime.Now;
                db.Invoices.Update(p);
                db.SaveChanges();
                p.InvoiceLines = db.InvoiceLines.Where(e => e.Invoice.Id == p.Id).Include(e => e.Product).ToList();
                var l = GetDTOLines(p);
                var res = p.toDTO();
                res.invoiceLines = l;
                return res;
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe invoice con el id " + id); }
            catch (Exception e) { throw e; }
        }


        public string DeleteInvoice(string id)
        {
            try
            {
                var invoicedto = db.Invoices.Where(p => p.DocumentNo == id).FirstOrDefault();
                if (invoicedto == null)
                    throw new Exception("No existe factura con el id " + id);
                db.Invoices.Remove(invoicedto);
                db.SaveChanges();
                return id;
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe factura con el id " + id); }
            catch (Exception e) { throw e; }
        }


        private Invoice newInvoice(InvoiceDTO dto)
        {
            try
            {
                Invoice c = new Invoice();
                c.DocumentNo = dto.DocumentNo;
                c.Description = dto.Description != null ? dto.Description : "";
                var date = DateTime.Parse(dto.ExchangeRateDate);
                var month = ExchangeRate.getMonthFormat(date);
                var day = ExchangeRate.convertDay(date.Day);
                var rate = db.ExchangeRates.Where(e => e.Month == month && e.Day == date.Day).First();
                if (rate == null)
                {
                    throw new Exception("Fecha de tipo de cambio inválida");
                }
                c.ExchangeRate = rate;
                c.IsDollar = dto.IsDollar;
                var client = db.Clients.Where(e => e.Code == dto.Client).FirstOrDefault();
                if (client == null)
                {
                    throw new Exception("No hay cliente con id " + dto.Client);
                }
                c.Client = client;
                c.InvoiceLines = buildLines(dto.invoiceLines, rate, dto.IsDollar);
                processTotals(c);
                return c;
            }
            catch (System.Exception)
            {

                throw;
            }

        }

        private Invoice invoiceToUpdate(Invoice c, InvoiceDTO dto)
        {
            try
            {
                var date = DateTime.Parse(dto.ExchangeRateDate);
                var month = ExchangeRate.getMonthFormat(date);
                var day = ExchangeRate.convertDay(date.Day);
                var rate = db.ExchangeRates.Where(e => e.Month == month && e.Day == date.Day).First();
                if (rate == null)
                {
                    throw new Exception("Fecha de tipo de cambio inválida");
                }
                if (c.ExchangeRate != rate)
                    c.ExchangeRate = rate;
                var client = db.Clients.Where(e => e.Code == dto.Client).FirstOrDefault();
                if (client == null)
                {
                    throw new Exception("No hay cliente con id " + dto.Client);
                }
                if (c.Client != client)
                    c.Client = client;
                if (c.IsDollar != dto.IsDollar)
                    c.IsDollar = dto.IsDollar;
                c.Description = dto.Description != null ? dto.Description : "";
                return c;
            }
            catch (Exception) { throw; }
        }

        private List<InvoiceLine> buildLines(List<InvoiceLineDTO> lines, ExchangeRate rate, bool isDollar)
        {
            try
            {
                List<InvoiceLine> list = new List<InvoiceLine>();
                for (int i = 0; i < lines.Count; i++)
                {
                    if (!checkLine(lines[i]))
                        throw new Exception("Uno o más valores no válidos en la línea " + i);
                    list.Add(newLine(lines[i], rate, isDollar));
                }
                return list;
            }
            catch (System.Exception) { throw; }

        }

        private void processTotals(Invoice invoice)
        {
            double gross = 0, net = 0;
            foreach (var item in invoice.InvoiceLines)
            {
                gross += item.LineGross;
                net += item.LineNet;
            }
            invoice.GrossPrice = gross;
            invoice.NetPrice = net;
        }

        private InvoiceLine newLine(InvoiceLineDTO dto, ExchangeRate rate, bool isDollar)
        {
            InvoiceLine invoiceLine = new InvoiceLine();
            invoiceLine.Amount = dto.Amount;
            invoiceLine.Unit = dto.Unit;
            invoiceLine.Description = dto.Description;
            invoiceLine.Exempt = dto.Exempt;
            var product = db.Products.Where(e => e.SKU == dto.Product).FirstOrDefault();
            if (product == null)
            {
                throw new Exception("No se encontró el producto con id " + dto.Product);
            }
            invoiceLine.Product = product;
            var unitPrice = getPriceToDate(product, rate, isDollar);
            invoiceLine.LineGross = dto.Amount * unitPrice;
            invoiceLine.LineNet = dto.Exempt ? invoiceLine.LineGross : invoiceLine.LineGross * 1.15;
            return invoiceLine;
        }

        private double getPriceToDate(Product product, ExchangeRate rate, Boolean isDollar)
        {
            double gross = 0;
            if (!isDollar && product.IsDollarPrice)
            {
                gross = product.NormalPrice * rate.Value;
            }
            else if (isDollar && !product.IsDollarPrice)
            {
                gross = product.NormalPrice / rate.Value;
            }
            else gross = product.NormalPrice;
            return gross;
        }

        private bool CheckInvoice(InvoiceDTO dto)
        {
            if (dto.DocumentNo != null && dto.DocumentNo != null && dto.Client != null && dto.Client != "" && dto.ExchangeRateDate != null && dto.ExchangeRateDate != "")
                return true;
            else return false;
        }

        private bool checkLine(InvoiceLineDTO dto)
        {
            if (dto.Product != null
                && dto.Product != ""
                && dto.Amount != 0
                && dto.Unit != null && dto.Unit != "")
                return true;
            else
                return false;
        }

        private List<InvoiceLineDTO> GetDTOLines(Invoice invoice)
        {
            List<InvoiceLineDTO> lineDTOs = new List<InvoiceLineDTO>();
            var lines = db.InvoiceLines.Where(e => e.Invoice.Id == invoice.Id).Include(e => e.Product).ToList();
            foreach (var line in lines)
            {
                lineDTOs.Add(line.toDTO());
            }
            return lineDTOs;
        }

        public void getReport()
        {
            var list = from c in db.Clients.Include(e => e.invoices).ToList()

                       select new
                       {
                           clientId = c.Id,
                           clientName = c.Name,
                           YearGroups = from o in c.invoices.ToList()
                                        group o by o.Created.Year into yg
                                        select new
                                        {
                                            Year = yg.Key,
                                            MonthGroups = from o in yg
                                                          group o by o.Created.Month into mg
                                                          select new { Month = mg.Key, Orders = mg }
                                        }.MonthGroups.ToList()
                       };
            JArray a = new JArray();
            foreach (var item in list)
            {
                JObject o = new JObject();
                o.Add("clientId", item.clientId);
                o.Add("clientName", item.clientName);
                a.Add(o);
                Console.WriteLine(item.clientId + " " + item.clientName);
            }
        }
    }
}
