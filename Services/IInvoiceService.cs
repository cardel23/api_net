using System;
using System.Collections.Generic;
using ser_api.Models;

namespace ser_api.Services
{
    public interface IInvoiceService
    {
        
        public List<InvoiceDTO> GetInvoices();
        public List<InvoiceDTO> GetClientInvoices(string id);
        public InvoiceDTO GetInvoice(string id);

        public InvoiceDTO AddInvoice(InvoiceDTO invoiceItem);

        public InvoiceDTO UpdateInvoice(string id, InvoiceDTO invoiceItem);

        public string DeleteInvoice(string id);
        public void getReport();
    }
    
}