using System;
using System.Collections.Generic;
using ser_api.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
namespace ser_api.Services
{
    public class ExchangeRateService : IExchangeRateService
    {
        DBContext db;
        public ExchangeRateService(DBContext context)
        {
            this.db = context;
        }
        public List<ExchangeRateDTO> GetExchangeRates(String month)
        {
            var list = db.ExchangeRates.Where(e => e.Month == month).ToList();
            List<ExchangeRateDTO> dTOs = new List<ExchangeRateDTO>();
            if (list.Count > 0)
            {
                list.ForEach(e => dTOs.Add(e.toDTO()));
            }
            return dTOs;
        }
        public ExchangeRateDTO GetExchangeRate(string id)
        {
            try
            {
                return db.ExchangeRates.Where(p => p.Id == id).First().toDTO();
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException e) { throw e; }
            catch (Exception e) { throw e; }
        }

        public ExchangeRateDTO GetExchangeRate()
        {
            try
            {
                var date = DateTime.Now;
                var day = date.Day;
                var month = getMonthFormat(date);
                var rate = db.ExchangeRates.Where(e => e.Day == day && e.Month == month).First();
                if (rate != null)
                    return rate.toDTO();
                else throw new Exception("No existe el tipo de cambio en la fecha indicada");
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public ExchangeRateDTO AddExchangeRate(ExchangeRateDTO ExchangeRateItem)
        {
            try
            {
                var rate = newRate(ExchangeRateItem);
                db.ExchangeRates.Add(rate);
                db.SaveChanges();
                return rate.toDTO();
            }
            catch (FormatException) { throw; }
            catch (System.InvalidOperationException e) { throw e; }
            catch (DbUpdateException e)
            {
                throw e;
            }
        }

        public ExchangeRateDTO UpdateExchangeRate(string id, ExchangeRateDTO ExchangeRateItem)
        {
            try
            {
                var old = db.ExchangeRates.Where(e => e.Id == ExchangeRateItem.Id).First();
                if (old == null)
                {
                    throw new Exception("No existe tasa de cambio con el id " + ExchangeRateItem.Id);
                }
                ExchangeRate rate = rateToUpdate(old, ExchangeRateItem);
                rate.Updated = DateTime.Now;
                db.ExchangeRates.Update(rate);
                db.SaveChanges();
                return rate.toDTO();
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe tasa de cambio con el id " + ExchangeRateItem.Id); }
            catch (Exception e) { throw e; }
        }

        public string DeleteExchangeRate(string id)
        {
            try
            {
                var ExchangeRateDTO = db.ExchangeRates.Where(p => p.Id == id).First();
                if(ExchangeRateDTO == null){
                     throw new Exception("No existe tasa de cambio con el id " + id);
                }
                db.ExchangeRates.Remove(ExchangeRateDTO);
                db.SaveChanges();
                return id;
                
            }
            catch (System.NullReferenceException e) { throw e; }
            catch (System.InvalidOperationException) { throw new Exception("No existe tasa de cambio con el id " + id); }
            catch (Exception e) { throw e; }
        }

        private ExchangeRate newRate(ExchangeRateDTO dto)
        {
            try
            {
                var date = CheckDate(dto);
                ExchangeRate rate = new ExchangeRate();
                rate.Day = date.Day;
                rate.Month = getMonthFormat(date);
                rate.Value = dto.Value;
                return rate;
            }
            catch (Exception) { throw; }
        }

        private ExchangeRate rateToUpdate(ExchangeRate rate, ExchangeRateDTO dto)
        {
            try
            {
                var date = CheckDate(dto);
                var month = getMonthFormat(date);
                if (rate.Day != date.Day)
                    rate.Day = date.Day;
                if (rate.Month != month)
                    rate.Month = month;
                if (rate.Value != dto.Value)
                    rate.Value = dto.Value;
                return rate;
            }
            catch (Exception) { throw; }

        }


        private DateTime CheckDate(ExchangeRateDTO dto)
        {
            try
            {
                var date = Convert.ToDateTime(dto.Date);
                return date;
            }
            catch (FormatException e)
            {
                throw e;
            }
        }

        private string getMonthFormat(DateTime date)
        {
            return date.Month < 10 ? date.Year.ToString() + "-0" + date.Month.ToString() : date.Year.ToString() + "-" + date.Month.ToString();
        }
    }

}