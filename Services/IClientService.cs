using System;
using System.Collections.Generic;
using ser_api.Models;

namespace ser_api.Services
{
    public interface IClientService
    {
        
        public List<ClientDTO> GetClients();

        public ClientDTO GetClient(String id);
        
        public ClientDTO AddClient(ClientDTO ClientItem);

        public ClientDTO UpdateClient(string id, ClientDTO ClientItem);

        public string DeleteClient(string id);
    }
    
}