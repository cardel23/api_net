﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ser_api.Migrations
{
    public partial class seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false),
                    Code = table.Column<string>(type: "varchar(13)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExchangeRates",
                columns: table => new
                {
                    Id = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Value = table.Column<double>(nullable: false),
                    Month = table.Column<string>(nullable: false),
                    Day = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false),
                    Description = table.Column<string>(nullable: true),
                    SKU = table.Column<string>(type: "varchar(13)", nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    NormalPrice = table.Column<double>(nullable: false),
                    IsDollarPrice = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                columns: table => new
                {
                    Id = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    ClientId = table.Column<string>(nullable: true),
                    DocumentNo = table.Column<string>(type: "varchar(30)", nullable: false),
                    GrossPrice = table.Column<double>(nullable: false),
                    NetPrice = table.Column<double>(nullable: false),
                    ExchangeRateId = table.Column<string>(nullable: true),
                    IsDollar = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Invoices_ExchangeRates_ExchangeRateId",
                        column: x => x.ExchangeRateId,
                        principalTable: "ExchangeRates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceLines",
                columns: table => new
                {
                    Id = table.Column<string>(type: "NVARCHAR(32)", nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    ProductId = table.Column<string>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Unit = table.Column<string>(nullable: true),
                    LineGross = table.Column<double>(nullable: false),
                    LineNet = table.Column<double>(nullable: false),
                    InvoiceId = table.Column<string>(nullable: false),
                    Exempt = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InvoiceLines_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvoiceLines_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "Code", "Created", "Name", "Updated" },
                values: new object[,]
                {
                    { "0982ABF771054E3499ADEA83AABFDC1A", "carlosd", new DateTime(2020, 10, 19, 3, 25, 42, 349, DateTimeKind.Local).AddTicks(9094), "Carlos DC", new DateTime(2020, 10, 19, 3, 25, 42, 349, DateTimeKind.Local).AddTicks(9126) },
                    { "0982ABF771054E3499ADEA83AABFDC1B", "kurtd", new DateTime(2020, 10, 19, 3, 25, 42, 349, DateTimeKind.Local).AddTicks(9963), "Kurt D", new DateTime(2020, 10, 19, 3, 25, 42, 349, DateTimeKind.Local).AddTicks(9965) },
                    { "0982ABF771054E3499ADEA83AABFDC1C", "nicolem", new DateTime(2020, 10, 19, 3, 25, 42, 350, DateTimeKind.Local).AddTicks(11), "Nicole M", new DateTime(2020, 10, 19, 3, 25, 42, 350, DateTimeKind.Local).AddTicks(12) },
                    { "0982ABF771054E3499ADEA83AABFDC1D", "taniat", new DateTime(2020, 10, 19, 3, 25, 42, 350, DateTimeKind.Local).AddTicks(39), "Tania T", new DateTime(2020, 10, 19, 3, 25, 42, 350, DateTimeKind.Local).AddTicks(41) },
                    { "E89E9F9594734315958309E7BE32BB50", "andresc", new DateTime(2020, 10, 19, 3, 25, 42, 350, DateTimeKind.Local).AddTicks(66), "Andres C", new DateTime(2020, 10, 19, 3, 25, 42, 350, DateTimeKind.Local).AddTicks(68) }
                });

            migrationBuilder.InsertData(
                table: "ExchangeRates",
                columns: new[] { "Id", "Created", "Day", "Month", "Updated", "Value" },
                values: new object[,]
                {
                    { "14-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 14, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.5505 },
                    { "13-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 13, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.547699999999999 },
                    { "12-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 12, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.544899999999998 },
                    { "11-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 11, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.542200000000001 },
                    { "10-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 10, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.539400000000001 },
                    { "09-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 9, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.5366 },
                    { "07-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 7, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.530999999999999 },
                    { "15-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 15, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.5533 },
                    { "06-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.528199999999998 },
                    { "05-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.525399999999998 },
                    { "04-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.522599999999997 },
                    { "03-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.519799999999996 },
                    { "08-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 8, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.533799999999999 },
                    { "16-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 16, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.556100000000001 },
                    { "18-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 18, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.561700000000002 },
                    { "02-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.517099999999999 },
                    { "19-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 19, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.564500000000002 },
                    { "20-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.567300000000003 },
                    { "21-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.570099999999996 },
                    { "22-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.572899999999997 },
                    { "23-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 23, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.575699999999998 },
                    { "24-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 24, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.578400000000002 },
                    { "25-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 25, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.581200000000003 },
                    { "26-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 26, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.584000000000003 },
                    { "27-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 27, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.586799999999997 },
                    { "28-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 28, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.589599999999997 },
                    { "29-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 29, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.592399999999998 },
                    { "30-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 30, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.595199999999998 },
                    { "17-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 17, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.558900000000001 },
                    { "01-09-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "2020-09", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.514299999999999 },
                    { "30-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 30, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.679099999999998 },
                    { "14-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 14, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.634300000000003 },
                    { "02-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.6008 },
                    { "03-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.6036 },
                    { "04-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.606400000000001 },
                    { "05-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.609200000000001 },
                    { "06-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 6, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.612000000000002 },
                    { "07-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 7, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.614800000000002 },
                    { "08-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 8, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.617600000000003 },
                    { "09-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 9, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.620399999999997 },
                    { "10-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 10, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.623199999999997 },
                    { "11-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 11, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.625999999999998 },
                    { "12-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 12, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.628700000000002 },
                    { "13-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 13, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.631500000000003 },
                    { "31-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 31, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.681899999999999 },
                    { "01-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.597999999999999 },
                    { "15-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 15, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.637099999999997 },
                    { "17-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 17, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.642699999999998 },
                    { "18-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 18, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.645499999999998 },
                    { "19-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 19, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.648299999999999 },
                    { "20-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 20, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.6511 },
                    { "21-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 21, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.6539 },
                    { "22-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 22, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.656700000000001 },
                    { "23-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 23, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.659500000000001 },
                    { "24-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 24, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.662300000000002 },
                    { "25-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 25, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.665100000000002 },
                    { "26-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 26, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.667900000000003 },
                    { "27-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 27, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.670699999999997 },
                    { "28-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 28, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.673499999999997 },
                    { "29-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 29, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.676299999999998 },
                    { "16-10-2020", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 16, "2020-10", new DateTime(2020, 10, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 34.639899999999997 }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Active", "Created", "Description", "IsDollarPrice", "Name", "NormalPrice", "SKU", "Updated" },
                values: new object[,]
                {
                    { "2C84499AA0A7411BA7063FC4A4AE06A8", true, new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(2710), "Descripcion", true, "Producto 4", 36.850000000000001, "product4", new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(2713) },
                    { "865425D3A0BD478C8A7617D81E5BAAD9", true, new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(2785), "Descripcion en español", false, "Producto 2", 420.69, "product2", new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(2787) },
                    { "219003F6DE66455AA0899F36F69C2265", true, new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(1130), "Descripcion en español", true, "Producto 1", 39.689999999999998, "product1", new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(1137) },
                    { "FFBF415DB1A1445E97409557988FF5A0", true, new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(2812), "Descripcion", false, "Producto 3", 750.0, "product3", new DateTime(2020, 10, 19, 3, 25, 42, 365, DateTimeKind.Local).AddTicks(2813) }
                });

            migrationBuilder.InsertData(
                table: "Invoices",
                columns: new[] { "Id", "ClientId", "Created", "Description", "DocumentNo", "ExchangeRateId", "GrossPrice", "IsDollar", "NetPrice", "Updated" },
                values: new object[] { "5AEAA4D3F9E546E1B5C1A6B32E25CD14", "0982ABF771054E3499ADEA83AABFDC1A", new DateTime(2020, 10, 19, 3, 25, 42, 372, DateTimeKind.Local).AddTicks(2145), "", "factura2", "18-10-2020", 312.94943975408103, true, 337.84513958522803, new DateTime(2020, 10, 19, 3, 25, 42, 372, DateTimeKind.Local).AddTicks(2147) });

            migrationBuilder.InsertData(
                table: "Invoices",
                columns: new[] { "Id", "ClientId", "Created", "Description", "DocumentNo", "ExchangeRateId", "GrossPrice", "IsDollar", "NetPrice", "Updated" },
                values: new object[] { "9A864FB46B034D27AD6366D5C3429593", "0982ABF771054E3499ADEA83AABFDC1A", new DateTime(2020, 10, 19, 3, 25, 42, 371, DateTimeKind.Local).AddTicks(9953), "", "factura1", "19-10-2020", 12324.112501, false, 13216.393414, new DateTime(2020, 10, 19, 3, 25, 42, 371, DateTimeKind.Local).AddTicks(9956) });

            migrationBuilder.InsertData(
                table: "InvoiceLines",
                columns: new[] { "Id", "Amount", "Created", "Description", "Exempt", "InvoiceId", "LineGross", "LineNet", "ProductId", "Unit", "Updated" },
                values: new object[,]
                {
                    { "35F969A4BD2E469EB7E22F418982DECA", 3.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(7923), "", true, "5AEAA4D3F9E546E1B5C1A6B32E25CD14", 110.55, 110.55, "2C84499AA0A7411BA7063FC4A4AE06A8", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(7925) },
                    { "819BE67D16BB4AF593BB91781E0C61BD", 4.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8066), "", false, "5AEAA4D3F9E546E1B5C1A6B32E25CD14", 86.591332207646005, 99.580032038792893, "FFBF415DB1A1445E97409557988FF5A0", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8068) },
                    { "F8D8973CC19D4FD495334EBFF7C51E5E", 2.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8147), "", false, "5AEAA4D3F9E546E1B5C1A6B32E25CD14", 79.379999999999995, 91.287000000000006, "219003F6DE66455AA0899F36F69C2265", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8148) },
                    { "FF99571302A143A1930AE6799F84996C", 3.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8173), "", true, "5AEAA4D3F9E546E1B5C1A6B32E25CD14", 36.428107546434603, 36.428107546434603, "865425D3A0BD478C8A7617D81E5BAAD9", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8175) },
                    { "11693EC2C93140E49165CCAC7015424E", 4.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(5192), "", false, "9A864FB46B034D27AD6366D5C3429593", 5107.15942, 5873.2333330000001, "2C84499AA0A7411BA7063FC4A4AE06A8", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(5194) },
                    { "3F6B676A71F1432789ABAC8667926EA7", 3.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8037), "", true, "9A864FB46B034D27AD6366D5C3429593", 2250.0, 2250.0, "FFBF415DB1A1445E97409557988FF5A0", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8039) },
                    { "94FFB7BDF8A04BA8843B71F2F9A6B870", 3.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8093), "", true, "9A864FB46B034D27AD6366D5C3429593", 4125.5730809999995, 4125.5730809999995, "219003F6DE66455AA0899F36F69C2265", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8095) },
                    { "A0EDDDDC91CE42AEB5F7F42C1979EAEA", 2.0, new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8120), "", false, "9A864FB46B034D27AD6366D5C3429593", 841.38, 967.58699999999999, "865425D3A0BD478C8A7617D81E5BAAD9", "unidad", new DateTime(2020, 10, 19, 3, 25, 42, 377, DateTimeKind.Local).AddTicks(8122) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_Month_Day",
                table: "ExchangeRates",
                columns: new[] { "Month", "Day" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceLines_InvoiceId",
                table: "InvoiceLines",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceLines_ProductId",
                table: "InvoiceLines",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ClientId",
                table: "Invoices",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_DocumentNo",
                table: "Invoices",
                column: "DocumentNo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_ExchangeRateId",
                table: "Invoices",
                column: "ExchangeRateId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SKU",
                table: "Products",
                column: "SKU",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InvoiceLines");

            migrationBuilder.DropTable(
                name: "Invoices");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "ExchangeRates");
        }
    }
}
