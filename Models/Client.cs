using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace ser_api.Models
{
    public class Client : Model
    {
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(13)")]
        public string Code {get; set;}
        public List<Invoice> invoices {get; set;}
        public ClientDTO toDTO(){
            return new ClientDTO(){
                Id = this.Id,
                Created = this.Created.ToString(),
                Updated = this.Updated.ToString(),
                Name = this.Name,
                Code = this.Code
            };
        }
    }

    public class ClientDTO {
        public string Id;
        public string Name;
        public string Code;
        public string Created;
        public string Updated;
    }
}