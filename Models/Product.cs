using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace ser_api.Models
{
    public class Product : Model {
        [Column(TypeName = "varchar(50)")]
        public string Name {get; set;}
        public string Description {get; set;}
        [Column(TypeName = "varchar(13)")]
        public string SKU {get; set;}
        public bool Active {get; set;}
        public double NormalPrice {get; set;}
        // public double? DollarPrice {get; set;}
        public bool IsDollarPrice {get; set;}

        public ProductDTO toDTO(ExchangeRateDTO rate){
            double convertedPrice = 0;
            if(IsDollarPrice){
                convertedPrice = NormalPrice*rate.Value;
            } else {
                convertedPrice = NormalPrice/rate.Value;
            }
            return new ProductDTO(){
                Id = this.Id,
                Name = this.Name,
                Description = this.Description,
                SKU = this.SKU,
                Active = this.Active,
                NormalPrice = this.NormalPrice,
                ConvertedPrice = convertedPrice,
                IsDollarPrice = this.IsDollarPrice,
                ExchangeRate = rate.ToString(),
                Created = this.Created.ToString(),
                Updated = this.Updated.ToString()
            };
        }
        public ProductDTO toDTO(){
      
            return new ProductDTO(){
                Id = this.Id,
                Name = this.Name,
                Description = this.Description,
                SKU = this.SKU,
                Active = this.Active,
                NormalPrice = this.NormalPrice,
                IsDollarPrice = this.IsDollarPrice,
                Created = this.Created.ToString(),
                Updated = this.Updated.ToString()
            };
        }
    }

    public class ProductDTO {
        public string Id;
        public string Name;
        public string Description;
        public string SKU;
        public bool Active;
        public double NormalPrice;
        public double ConvertedPrice;
        public bool IsDollarPrice;
        public string ExchangeRate;
        public string Created;
        public string Updated;
    }
}