using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
namespace ser_api.Models
{
    public abstract class Model {

        public Model(){
            Id = System.Guid.NewGuid().ToString().Replace("-","").ToUpper();
            Created = DateTime.Now;
            Updated = Created;
        }
        [Column(TypeName = "NVARCHAR(32)")]
        public string Id { get; set; }
        public DateTime Created {get; set;}
        public DateTime Updated {get; set;}

        public override string ToString(){
            return Id;
        }
    }
    
}