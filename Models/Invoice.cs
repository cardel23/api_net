using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ser_api.Models
{
    public class Invoice : Model
    {
        [ForeignKey("ClientId")]
        public Client Client { get; set; }
        public string ClientId {get; set;}

        [Column(TypeName = "varchar(30)")]
        public string DocumentNo { get; set; }

        public ICollection<InvoiceLine> InvoiceLines { get; set; }

        public double GrossPrice { get; set; }

        public double NetPrice { get; set; }

        [ForeignKey("ExchangeRateId")]
        public ExchangeRate ExchangeRate {get; set;}
        public string ExchangeRateId {get; set;}
        public bool IsDollar {get; set;}

        [Column(TypeName = "varchar(255)")]
        public string Description {get; set;}

        public InvoiceDTO toDTO()
        {
            var exdto = this.ExchangeRate.toDTO();
            double convertedGP = 0, convertedNP = 0;
            if(this.IsDollar){
                convertedGP = this.GrossPrice*exdto.Value;
                convertedNP = this.NetPrice*exdto.Value;
            } else {
                convertedGP = this.GrossPrice/exdto.Value;
                convertedNP = this.NetPrice/exdto.Value;
            }
            return new InvoiceDTO()
            {
                
                Id = this.Id,
                Created = this.Created.ToString(),
                Updated = this.Updated.ToString(),
                DocumentNo = this.DocumentNo,
                ExchangeRateDate = exdto.Date,
                ExchangeRate = exdto.Value,
                Client = this.Client.Code,
                IsDollar = this.IsDollar,
                GrossPrice = this.GrossPrice,
                NetPrice = this.NetPrice,
                ConvertedGrossPrice = convertedGP,
                ConvertedNetPrice = convertedNP,
                Description = this.Description!=null?this.Description:""
            };
        }
    }

    public class InvoiceDTO
    {
        public string Id;
        public string DocumentNo;
        public string Created;
        public string Updated;
        public double ExchangeRate;
        public string ExchangeRateDate;
        public string Client;
        public double GrossPrice;
        public double NetPrice;
        public double ConvertedGrossPrice;
        public double ConvertedNetPrice;
        public bool IsDollar;
        public string Description;
        public List<InvoiceLineDTO> invoiceLines;
    }
}