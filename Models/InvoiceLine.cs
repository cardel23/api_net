using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ser_api.Models
{
    public class InvoiceLine : Model
    {
        [ForeignKey("ProductId")]
        public Product Product { get; set; }
        public string ProductId { get; set; }
        public double Amount { get; set; }
        public string Unit { get; set; }
        public double LineGross { get; set; }
        public double LineNet { get; set; }
        [ForeignKey("InvoiceId")]
        public Invoice Invoice { get; set; }
        public string InvoiceId { get; set; }
        public bool Exempt {get; set;}
        
        [Column(TypeName = "varchar(255)")]
        public string Description {get; set;}

        public InvoiceLineDTO toDTO(){
            return new InvoiceLineDTO(){
                Id = this.Id,
                Created = this.Created.ToString(),
                Updated = this.Updated.ToString(),
                Product = this.Product.SKU,
                Invoice = this.Invoice.Id,
                Amount = this.Amount,
                Unit = this.Unit,
                Description = this.Description!=null?this.Description:"",
                LineGross = this.LineGross,
                LineNet = this.LineNet
            };
        }

    }

    public class InvoiceLineDTO{
        public string Id;        
        public string Created;
        public string Updated;
        public string Product;
        public string Invoice;
        public bool Exempt;
        public double Amount;
        public string Unit;
        public double LineGross;
        public double LineNet;
        public string Description;

    }
}