using System;
using System.Collections.Generic;

namespace ser_api.Models
{
    public class ExchangeRate : Model
    {
        public double Value { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
        // public List<Product> Products { get; set; }

        public override string ToString(){
            return Id+" - "+Value;
        }

        public ExchangeRateDTO toDTO(){
            return new ExchangeRateDTO(){
                Id = this.Id,
                Created = this.Created.ToString(),
                Updated = this.Updated.ToString(),
                Date = this.Month+"-"+convertDay(this.Day),
                Value = this.Value
            };
        }

        public static string getMonthFormat(DateTime date)
        {
            return date.Month < 10 ? date.Year.ToString() + "-0" + date.Month.ToString() : date.Year.ToString() + "-" + date.Month.ToString();
        }
        public static string convertDay(int day){
            return day<10?"0"+day:day.ToString();
        }
    }

    public class ExchangeRateDTO {
        public string Id;
        public double Value;
        public string Date;
        public string Created;
        public string Updated;

        public override string ToString(){
            return Value+" - "+Date;
        }
    }
    
}