using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
namespace ser_api.Models
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options) { }
        public DbSet<Product> Products { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceLine> InvoiceLines { get; set; }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //     => optionsBuilder.UseNpgsql("Host=localhost;Database=ser_api;Username=tad;Password=tad");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).IsRequired();
                entity.HasData(
                    new Client { Id = "0982ABF771054E3499ADEA83AABFDC1A", Created = DateTime.Now, Updated = DateTime.Now, Name = "Carlos DC", Code = "carlosd" },
                    new Client { Id = "0982ABF771054E3499ADEA83AABFDC1B", Created = DateTime.Now, Updated = DateTime.Now, Name = "Kurt D", Code = "kurtd" },
                    new Client { Id = "0982ABF771054E3499ADEA83AABFDC1C", Created = DateTime.Now, Updated = DateTime.Now, Name = "Nicole M", Code = "nicolem" },
                    new Client { Id = "0982ABF771054E3499ADEA83AABFDC1D", Created = DateTime.Now, Updated = DateTime.Now, Name = "Tania T", Code = "taniat" },
                    new Client { Id = "E89E9F9594734315958309E7BE32BB50", Created = DateTime.Now, Updated = DateTime.Now, Name = "Andres C", Code = "andresc" }
                );
            });

            modelBuilder.Entity<ExchangeRate>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Value).IsRequired();
                entity.Property(e => e.Month).IsRequired();
                entity.Property(e => e.Day).IsRequired();
                entity.HasIndex(e => new { e.Month, e.Day }).IsUnique();
                entity.HasData(
                    new ExchangeRate { Id = "01-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.598, Month = "2020-10", Day = 1 },
                    new ExchangeRate { Id = "02-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6008, Month = "2020-10", Day = 2 },
                    new ExchangeRate { Id = "03-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6036, Month = "2020-10", Day = 3 },
                    new ExchangeRate { Id = "04-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6064, Month = "2020-10", Day = 4 },
                    new ExchangeRate { Id = "05-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6092, Month = "2020-10", Day = 5 },
                    new ExchangeRate { Id = "06-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.612, Month = "2020-10", Day = 6 },
                    new ExchangeRate { Id = "07-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6148, Month = "2020-10", Day = 7 },
                    new ExchangeRate { Id = "08-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6176, Month = "2020-10", Day = 8 },
                    new ExchangeRate { Id = "09-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6204, Month = "2020-10", Day = 9 },
                    new ExchangeRate { Id = "10-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6232, Month = "2020-10", Day = 10 },
                    new ExchangeRate { Id = "11-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.626, Month = "2020-10", Day = 11 },
                    new ExchangeRate { Id = "12-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6287, Month = "2020-10", Day = 12 },
                    new ExchangeRate { Id = "13-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6315, Month = "2020-10", Day = 13 },
                    new ExchangeRate { Id = "14-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6343, Month = "2020-10", Day = 14 },
                    new ExchangeRate { Id = "15-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6371, Month = "2020-10", Day = 15 },
                    new ExchangeRate { Id = "16-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6399, Month = "2020-10", Day = 16 },
                    new ExchangeRate { Id = "17-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6427, Month = "2020-10", Day = 17 },
                    new ExchangeRate { Id = "18-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6455, Month = "2020-10", Day = 18 },
                    new ExchangeRate { Id = "19-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6483, Month = "2020-10", Day = 19 },
                    new ExchangeRate { Id = "20-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6511, Month = "2020-10", Day = 20 },
                    new ExchangeRate { Id = "21-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6539, Month = "2020-10", Day = 21 },
                    new ExchangeRate { Id = "22-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6567, Month = "2020-10", Day = 22 },
                    new ExchangeRate { Id = "23-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6595, Month = "2020-10", Day = 23 },
                    new ExchangeRate { Id = "24-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6623, Month = "2020-10", Day = 24 },
                    new ExchangeRate { Id = "25-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6651, Month = "2020-10", Day = 25 },
                    new ExchangeRate { Id = "26-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6679, Month = "2020-10", Day = 26 },
                    new ExchangeRate { Id = "27-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6707, Month = "2020-10", Day = 27 },
                    new ExchangeRate { Id = "28-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6735, Month = "2020-10", Day = 28 },
                    new ExchangeRate { Id = "29-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6763, Month = "2020-10", Day = 29 },
                    new ExchangeRate { Id = "30-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6791, Month = "2020-10", Day = 30 },
                    new ExchangeRate { Id = "31-10-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.6819, Month = "2020-10", Day = 31 },
                    new ExchangeRate { Id = "01-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5143, Month = "2020-09", Day = 1 },
                    new ExchangeRate { Id = "02-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5171, Month = "2020-09", Day = 2 },
                    new ExchangeRate { Id = "03-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5198, Month = "2020-09", Day = 3 },
                    new ExchangeRate { Id = "04-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5226, Month = "2020-09", Day = 4 },
                    new ExchangeRate { Id = "05-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5254, Month = "2020-09", Day = 5 },
                    new ExchangeRate { Id = "06-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5282, Month = "2020-09", Day = 6 },
                    new ExchangeRate { Id = "07-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.531, Month = "2020-09", Day = 7 },
                    new ExchangeRate { Id = "08-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5338, Month = "2020-09", Day = 8 },
                    new ExchangeRate { Id = "09-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5366, Month = "2020-09", Day = 9 },
                    new ExchangeRate { Id = "10-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5394, Month = "2020-09", Day = 10 },
                    new ExchangeRate { Id = "11-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5422, Month = "2020-09", Day = 11 },
                    new ExchangeRate { Id = "12-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5449, Month = "2020-09", Day = 12 },
                    new ExchangeRate { Id = "13-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5477, Month = "2020-09", Day = 13 },
                    new ExchangeRate { Id = "14-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5505, Month = "2020-09", Day = 14 },
                    new ExchangeRate { Id = "15-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5533, Month = "2020-09", Day = 15 },
                    new ExchangeRate { Id = "16-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5561, Month = "2020-09", Day = 16 },
                    new ExchangeRate { Id = "17-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5589, Month = "2020-09", Day = 17 },
                    new ExchangeRate { Id = "18-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5617, Month = "2020-09", Day = 18 },
                    new ExchangeRate { Id = "19-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5645, Month = "2020-09", Day = 19 },
                    new ExchangeRate { Id = "20-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5673, Month = "2020-09", Day = 20 },
                    new ExchangeRate { Id = "21-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5701, Month = "2020-09", Day = 21 },
                    new ExchangeRate { Id = "22-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5729, Month = "2020-09", Day = 22 },
                    new ExchangeRate { Id = "23-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5757, Month = "2020-09", Day = 23 },
                    new ExchangeRate { Id = "24-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5784, Month = "2020-09", Day = 24 },
                    new ExchangeRate { Id = "25-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5812, Month = "2020-09", Day = 25 },
                    new ExchangeRate { Id = "26-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.584, Month = "2020-09", Day = 26 },
                    new ExchangeRate { Id = "27-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5868, Month = "2020-09", Day = 27 },
                    new ExchangeRate { Id = "28-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5896, Month = "2020-09", Day = 28 },
                    new ExchangeRate { Id = "29-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5924, Month = "2020-09", Day = 29 },
                    new ExchangeRate { Id = "30-09-2020", Created = DateTime.Parse("2020-10-17"), Updated = DateTime.Parse("2020-10-17"), Value = 34.5952, Month = "2020-09", Day = 30 }
                );
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.SKU).IsRequired();
                entity.Property(e => e.Name).IsRequired();
                entity.HasIndex(e => e.SKU).IsUnique();

                entity.HasData(
                    new Product{SKU="product1",Id="219003F6DE66455AA0899F36F69C2265",Created=DateTime.Now,Updated=DateTime.Now,Name="Producto 1",Description="Descripcion en español",Active=true,NormalPrice=39.69,IsDollarPrice=true},
                    new Product{SKU="product4",Id="2C84499AA0A7411BA7063FC4A4AE06A8",Created=DateTime.Now,Updated=DateTime.Now,Name="Producto 4",Description="Descripcion",Active=true,NormalPrice=36.85,IsDollarPrice=true},
                    new Product{SKU="product2",Id="865425D3A0BD478C8A7617D81E5BAAD9",Created=DateTime.Now,Updated=DateTime.Now,Name="Producto 2",Description="Descripcion en español",Active=true,NormalPrice=420.69,IsDollarPrice=false},
                    new Product{SKU="product3",Id="FFBF415DB1A1445E97409557988FF5A0",Created=DateTime.Now,Updated=DateTime.Now,Name="Producto 3",Description="Descripcion",Active=true,NormalPrice=750,IsDollarPrice=false}
                );
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.DocumentNo).IsRequired();
                entity.HasIndex(e => e.DocumentNo).IsUnique();
                entity.HasMany(e => e.InvoiceLines).WithOne(e => e.Invoice).OnDelete(DeleteBehavior.Cascade);

                entity.HasData(
                    new Invoice{Id="9A864FB46B034D27AD6366D5C3429593",Created=DateTime.Now,Updated=DateTime.Now,ClientId="0982ABF771054E3499ADEA83AABFDC1A",GrossPrice=12324.112501,NetPrice=13216.393414,DocumentNo="factura1",ExchangeRateId="19-10-2020",IsDollar=false,Description=""},
                    new Invoice{Id="5AEAA4D3F9E546E1B5C1A6B32E25CD14",Created=DateTime.Now,Updated=DateTime.Now,ClientId="0982ABF771054E3499ADEA83AABFDC1A",GrossPrice=312.949439754081,NetPrice=337.845139585228,DocumentNo="factura2",ExchangeRateId="18-10-2020",IsDollar=true,Description=""}
                );

            });

            modelBuilder.Entity<InvoiceLine>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasOne(e => e.Invoice).WithMany(e => e.InvoiceLines).IsRequired();
                entity.HasOne(e => e.Product).WithMany().IsRequired().OnDelete(DeleteBehavior.Cascade);
                
                entity.HasData(
                    new InvoiceLine{Id="11693EC2C93140E49165CCAC7015424E",Created=DateTime.Now,Updated=DateTime.Now,ProductId="2C84499AA0A7411BA7063FC4A4AE06A8",Amount=4,Unit="unidad",LineGross=5107.15942,LineNet=5873.233333,InvoiceId="9A864FB46B034D27AD6366D5C3429593",Exempt=false,Description=""},
                    new InvoiceLine{Id="35F969A4BD2E469EB7E22F418982DECA",Created=DateTime.Now,Updated=DateTime.Now,ProductId="2C84499AA0A7411BA7063FC4A4AE06A8",Amount=3,Unit="unidad",LineGross=110.55,LineNet=110.55,InvoiceId="5AEAA4D3F9E546E1B5C1A6B32E25CD14",Exempt=true,Description=""},
                    new InvoiceLine{Id="3F6B676A71F1432789ABAC8667926EA7",Created=DateTime.Now,Updated=DateTime.Now,ProductId="FFBF415DB1A1445E97409557988FF5A0",Amount=3,Unit="unidad",LineGross=2250,LineNet=2250,InvoiceId="9A864FB46B034D27AD6366D5C3429593",Exempt=true,Description=""},
                    new InvoiceLine{Id="819BE67D16BB4AF593BB91781E0C61BD",Created=DateTime.Now,Updated=DateTime.Now,ProductId="FFBF415DB1A1445E97409557988FF5A0",Amount=4,Unit="unidad",LineGross=86.591332207646,LineNet=99.5800320387929,InvoiceId="5AEAA4D3F9E546E1B5C1A6B32E25CD14",Exempt=false,Description=""},
                    new InvoiceLine{Id="94FFB7BDF8A04BA8843B71F2F9A6B870",Created=DateTime.Now,Updated=DateTime.Now,ProductId="219003F6DE66455AA0899F36F69C2265",Amount=3,Unit="unidad",LineGross=4125.573081,LineNet=4125.573081,InvoiceId="9A864FB46B034D27AD6366D5C3429593",Exempt=true,Description=""},
                    new InvoiceLine{Id="A0EDDDDC91CE42AEB5F7F42C1979EAEA",Created=DateTime.Now,Updated=DateTime.Now,ProductId="865425D3A0BD478C8A7617D81E5BAAD9",Amount=2,Unit="unidad",LineGross=841.38,LineNet=967.587,InvoiceId="9A864FB46B034D27AD6366D5C3429593",Exempt=false,Description=""},
                    new InvoiceLine{Id="F8D8973CC19D4FD495334EBFF7C51E5E",Created=DateTime.Now,Updated=DateTime.Now,ProductId="219003F6DE66455AA0899F36F69C2265",Amount=2,Unit="unidad",LineGross=79.38,LineNet=91.287,InvoiceId="5AEAA4D3F9E546E1B5C1A6B32E25CD14",Exempt=false,Description=""},
                    new InvoiceLine{Id="FF99571302A143A1930AE6799F84996C",Created=DateTime.Now,Updated=DateTime.Now,ProductId="865425D3A0BD478C8A7617D81E5BAAD9",Amount=3,Unit="unidad",LineGross=36.4281075464346,LineNet=36.4281075464346,InvoiceId="5AEAA4D3F9E546E1B5C1A6B32E25CD14",Exempt=true,Description=""}
                );
            });
        }
    }


}